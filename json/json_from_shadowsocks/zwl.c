#include <stdio.h>
#include "json.h"
#include <stdlib.h>

void work(json_value* One) {
	if(!One) return;
	if (One->type == json_none || One->type == json_null) return;
	if (One->type == json_string) {
		printf("%s", (char*)One->u.string.ptr);
	} else if( One->type == json_integer ) {
		printf("%ld", One->u.integer);
	} else if( One->type == json_double) {
		printf("%f", One->u.dbl);
	} else if( One->type == json_object ) {
		printf("{\n");
		for(unsigned int i=0; i<One->u.object.length; ++i) {
			printf("%s:", One->u.object.values[i].name);
			work(One->u.object.values[i].value);
			if( i+1< One->u.object.length )
				printf(",\n");
			else
				printf("\n");
		}
		printf("}\n");
	} else if( One->type == json_array ) {
		printf("[\n");
		for(unsigned int i=0; i<One->u.array.length; ++i) {
			work(One->u.array.values[i]);
			if( i+1< One->u.array.length )
				printf(",");
			else
				printf("\n");
		}
		printf("]\n");
	}
}

int main(int argc, char**argv) {
	FILE* fp = fopen(argv[1],"r");
	fseek(fp,0,SEEK_END);
	unsigned int n = ftell(fp);
	fseek(fp,0,SEEK_SET);
	char* s = new char[n+1];
	fread(s, n, 1, fp);
	s[n] = 0;
	fclose(fp);

	json_value* a = json_parse( s, n-1);
	work(a);
	json_value_free(a);
	return 0;
}
